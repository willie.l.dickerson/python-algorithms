# Stephanie Ayala
# Homework 4
import string, linecache, re
import operator

def getActivities(st):
    # get array to store results
    activities=[]
    n = len(st)
    i = 0
    # add the activity with the latest start time
    activities.append(st[i][0])

    # Loop through the rest of the activies
    for j in range(1, n):
        # if the start time for the last stored activity is greater than or equal to the finish 
        # time of the next activity, add it
        if st[i][1] >= st[j][2]:
            activities.append(st[j][0])
            i = j
    return activities

# make sure act.out is empty
with open('act.out', "w"):
    pass

filename = "act.txt"
num_lines = sum(1 for line in open('act.txt'))
index = 1
count = 1
# while the file is being read
while(index <= num_lines):
    # get the number of activities
    N = int(linecache.getline(filename, index))
    index += 1
    Activity = []
    # get each activity as a list and add to activity list
    for i in range(N):
        F=[]
        ASF = linecache.getline(filename, index)
        ASF = re.split('\W+',ASF)

        F.append(int(ASF[0]))
        F.append(int(ASF[1]))
        F.append(int(ASF[2]))
        Activity.append(F)
        index += 1

    # sort the activity in descending order acording to start time
    Activity = sorted(Activity, key = operator.itemgetter(1), reverse=True)
    activities = getActivities(Activity)
    activities.reverse()


    # write to file
    with open('act.out', 'a') as fileout:
        fileout.write("Set ")
        fileout.write(str(count))
        fileout.write("\n")
        fileout.write("Number of activities selected = ")
        fileout.write(str(len(activities)))
        fileout.write("\n")
        fileout.write("Activities: ")
    for j in range(len(activities)):
        with open('act.out', 'a') as fileout:
            fileout.write(str(activities[j]))
            fileout.write(" ")
    with open('act.out', 'a') as fileout:
        fileout.write("\n")
        fileout.write("\n")
    count += 1

