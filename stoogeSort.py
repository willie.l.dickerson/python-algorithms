# Stephanie Ayala
# stoogesort - file input/output
import math
#!/usr/bin/python



# stoogesort function
def stoogesort(data, low, high):  
    n = high - low + 1
    # if n = 2 and A[0] > A[1]
    # swap A[0] and A[1]
    if n == 2 and data[low] > data[high]:
        data[low], data[high] = data[high], data[low]
  
    # if there are more than two elements
    elif n > 2:
        # m = ceilint(2n/3)
        mid = int(math.ceil((n * 2)/3))
        # StoogeSort(A[0 ... m - 1])
        # sort first 2/3 recursivly
        stoogesort(data, low, low+mid-1)
        # StoogeSort(A[n - m ... n - 1])
        # sort last 2/3 recursivly
        stoogesort(data, high - mid + 1, high)
        # Stoogesort(A[0 ... m - 1])
        # sort first 2/3 recursivly
        stoogesort(data, low, low+mid-1)



# create list
data = []
# read numbers into list, make a list of lists for each row
with open("data.txt") as file:
    for line in file:
        line = line.split()
        if line:
            data.append([int(i) for i in line])
# clear out file if it already exists
with open('stooge.out', "w"):
    pass
# for each element of the list (of lists)
for i in range(len(data)):
    arr = data[i]
    # dont use the first number
    arr = arr[1:]
    stoogesort(arr, 0, len(arr) - 1)
    # write numbers to file
    with open('stooge.out', 'a') as fileout:
        for d in arr:
            fileout.write("%i " % d)
        fileout.write("\n")


# https://www.tutorialspoint.com/python/python_files_io.htm
# https://www.tutorialspoint.com/python/time_clock.htm