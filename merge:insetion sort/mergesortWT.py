# Stephanie Ayala
# mergesort - Worst case input runtimes: lists in reverse order
#!/usr/bin/python
import time

# mergesort function
def mergesort(data):
    # if the list is greater than 1 then it needs to be sorted
    if len(data)>1:
        #initialize counters
        i, j, k = 0, 0, 0
        # find the middledle of the list
        middle = len(data)//2
        # split the data into two halves
        low = data[:middle]
        high = data[middle:]
        # recursivly sort both halves
        mergesort(low)
        mergesort(high)
        # while there is more than one element in each recursive list
        while i < len(low) and j < len(high):
            # sort the numbers and incriment the counters
            if low[i] < high[j]:
                data[k]=low[i]
                i += 1
            else: # if high is greater than or equal to low
                data[k]=high[j]
                j += 1
            k=k+1
        # copy low and high elements and incriment
        while i < len(low):
            data[k]=low[i]
            i +=1
            k +=1
        while j < len(high):
            data[k]=high[j]
            j +=1
            k +=1

# create array
data = []
# values start at the high end
data_value = 10000
# decrement the values until 0
while data_value != 0:
    # add value to list
    data.append(data_value)
    data_value -= 1  
# time before mergesort
t0 = time.clock()
mergesort(data)
# time after mergesort
t1 = time.clock()
t3 = t1 - t0
tiem1 = t3

data = []
data_value = 50000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time2 = t3

data = []
data_value = 100000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time3 = t3

data = []
data_value = 150000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time4 = t3

data = []
data_value = 200000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time5 = t3

data = []
data_value = 250000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time6 = t3

data = []
data_value = 300000

while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time7 = t3

data = []
data_value = 350000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time8 = t3

# write times to file
with open('mergesortWT.out', "w"):
    pass
with open('mergesortWT.out', 'a') as fileout:
    fileout.write("n = 10,000\n")
    fileout.write("Run Time = %f\n" % tiem1)
    fileout.write("n = 50,000\n")
    fileout.write("Run Time = %f\n" % time2)
    fileout.write("n = 100,000\n")
    fileout.write("Run Time = %f\n" % time3)
    fileout.write("n = 150,000\n")
    fileout.write("Run Time = %f\n" % time4)
    fileout.write("n = 200,000\n")
    fileout.write("Run Time = %f\n" % time5)
    fileout.write("n = 250,000\n")
    fileout.write("Run Time = %f\n" % time6)
    fileout.write("n = 300,000\n")
    fileout.write("Run Time = %f\n" % time7)
    fileout.write("n = 350,000\n")
    fileout.write("Run Time = %f\n" % time8)

# https://www.geeksforgeeks.org/merge-sort/
#  https://www.tutorialspoint.com/python/time_clock.htm
    