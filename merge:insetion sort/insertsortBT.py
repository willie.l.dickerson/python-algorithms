# Stephanie Ayala
# Insertion sort- Best case inputs: inputs of different sizes that are already sorted

#!/usr/bin/python
import time

# insertion sort function
def insertsort(data):
    size = len(data) 
    # go through each value
    for i in range(size):
        # set value to keyvalue
        keyval = data[i]
        # keep track of position
        pos = i
        # compare the keyvalue and switch if needed
        while pos > 0 and data[pos-1] > keyval:
            data[pos] = data[pos-1]
            pos = pos-1
            data[pos] = keyval

data = []
data_value = 0
data_max = 2000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time1 = t3

data = []
data_value = 0
data_max = 4000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time2 = t3

data = []
data_value = 0
data_max = 8000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time3 = t3

data = []
data_value = 0
data_max = 16000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time4 = t3

data = []
data_value = 0
data_max = 32000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time5 = t3

data = []
data_value = 0
data_max = 64000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time6 = t3

data = []
data_value = 0
data_max = 128000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time7 = t3

data = []
data_value = 0
data_max = 256000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time8 = t3


with open('insertBT.out', "w"):
    pass
with open('insertBT.out', 'a') as fileout:
    fileout.write("n = 2000\n")
    fileout.write("Run Time = %f\n" % time1)
    fileout.write("n = 4000\n")
    fileout.write("Run Time = %f\n" % time2)
    fileout.write("n = 8000\n")
    fileout.write("Run Time = %f\n" % time3)
    fileout.write("n = 16,000\n")
    fileout.write("Run Time = %f\n" % time4)
    fileout.write("n = 32,000\n")
    fileout.write("Run Time = %f\n" % time5)
    fileout.write("n = 64,000\n")
    fileout.write("Run Time = %f\n" % time6)
    fileout.write("n = 128,000\n")
    fileout.write("Run Time = %f\n" % time7)
    fileout.write("n = 256,000\n")
    fileout.write("Run Time = %f\n" % time8)

# https://www.tutorialspoint.com/python/time_clock.htm
# https://www.geeksforgeeks.org/insertion-sort/
# https://www.tutorialspoint.com/python/python_files_io.htm
